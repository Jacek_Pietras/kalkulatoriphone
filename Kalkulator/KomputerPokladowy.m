//
//  KomputerPokladowy.m
//  Kalkulator
//
//  Created by Jacek on 23/10/2018.
//  Copyright © 2018 student. All rights reserved.
//

#import "KomputerPokladowy.h"
#import "ViewController.h"

@implementation KomputerPokladowy
+ (double) spalanieDlaDystansu: (double) dystans iPaliwa: (double) paliwo{
    return paliwo * 100 / dystans;;
};
@end
