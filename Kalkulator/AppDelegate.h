//
//  AppDelegate.h
//  Kalkulator
//
//  Created by Jacek on 23/10/2018.
//  Copyright © 2018 student. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

