//
//  ViewController.h
//  Kalkulator
//
//  Created by Jacek on 23/10/2018.
//  Copyright © 2018 student. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *paliwoTextField;
@property (weak, nonatomic) IBOutlet UITextField *dystansTextField;
@property (weak, nonatomic) IBOutlet UILabel *wynikLabel;
- (IBAction)obliczButton:(id)sender;
- (IBAction)paliwoSliderChange:(id)sender;
- (IBAction)dystansSliderChange:(id)sender;
@property (weak, nonatomic) IBOutlet UISlider *paliwoSlider;
@property (weak, nonatomic) IBOutlet UISlider *dystansSlider;
- (IBAction)textFieldChange:(id)sender;


@end

