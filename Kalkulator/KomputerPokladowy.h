//
//  KomputerPokladowy.h
//  Kalkulator
//
//  Created by Jacek on 23/10/2018.
//  Copyright © 2018 student. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KomputerPokladowy : NSObject
+ (double) spalanieDlaDystansu: (double) dystans iPaliwa: (double) paliwo;
@end

NS_ASSUME_NONNULL_END
