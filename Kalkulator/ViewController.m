//
//  ViewController.m
//  Kalkulator
//
//  Created by Jacek on 23/10/2018.
//  Copyright © 2018 student. All rights reserved.
//

#import "ViewController.h"
#import "KomputerPokladowy.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.dystansTextField.delegate = self;
    self.paliwoTextField.delegate = self;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)obliczButton:(id)sender {
    double km = [self.dystansTextField.text doubleValue];
    double lt = [self.paliwoTextField.text doubleValue];
    
    if (km > 0) {
        double wynik = [KomputerPokladowy spalanieDlaDystansu:km iPaliwa:lt];
        
        if (wynik <= 6) {
            UIColor *color = [UIColor greenColor];
            [self.wynikLabel setTextColor: color];
            
        } else if (wynik <= 10) {
            self.wynikLabel.textColor = [UIColor yellowColor];
            
        } else {
            self.wynikLabel.textColor = [UIColor redColor];
        }
        
        self.wynikLabel.text =
        [NSString stringWithFormat:@"%.2f l/100km", wynik];
    } else {
        self.wynikLabel.text = @"błąd";
    }
}

- (IBAction)paliwoSliderChange:(id)sender {
    UISlider *senderSlider = (UISlider *) sender;
    self.paliwoTextField.text = [NSString stringWithFormat:@"%.0f",
        [senderSlider value]];
    [self obliczButton:nil];
}

- (IBAction)dystansSliderChange:(id)sender {
    UISlider *senderSlider = (UISlider *)sender;
    self.dystansTextField.text =
    [NSString stringWithFormat:@"%.0f",
     [senderSlider value]];
    [self obliczButton:nil];
}
- (IBAction)textFieldChange:(id)sender {
    self.dystansSlider.value =  [self.dystansTextField.text doubleValue];
    self.paliwoSlider.value =  [self.paliwoTextField.text doubleValue];
    [self obliczButton:nil];
}
@end
